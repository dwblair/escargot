import ujson as json
import urequests as requests
import time
import dht
import machine
from machine import Pin
import ssd1306
from machine import I2C
import onewire, ds18x20

index=0

i2c = I2C(-1, Pin(14), Pin(2))
oled = ssd1306.SSD1306_I2C(128, 64, i2c)


oled.fill(0)
oled.text("  Q U A H O G",0,0)
oled.show()
time.sleep(1)

url='http://172.16.10.119:4000'
headers = {'Content-type':'application/json', 'Accept':'application/json'}

essid="Artisan\'s Asylum"
psswd="learn.make.teach"
print(essid,psswd)



oled.fill(0)
oled.text("  Q U A H O G",0,0)
oled.text("Read sensors...",0,50)
oled.show()
time.sleep(1)


# SENSOR SETUP
dat = machine.Pin(5)
d = dht.DHT22(machine.Pin(18))



adc = machine.ADC(machine.Pin(35))

oled_str=" "


def post_data(payload):
    try:
    	r = requests.post(url,data=json.dumps(payload),headers=headers)
    except Exception as e:
	print(e)

	#r.close()

	return "timeout"

    else:

	r.close()

	print('Status', r.status_code)

   	return r.status_code



def do_connect(essid,psswd):
    import network
    sta_if = network.WLAN(network.STA_IF)
    if not sta_if.isconnected():
        print('connecting to network...')
        sta_if.active(False)
        sta_if.active(True)
        sta_if.connect(essid, psswd)
        while not sta_if.isconnected():
            pass
    print('network config:', sta_if.ifconfig())


while True:
    
    #dht22:
    d.measure()
    t=d.temperature()
    h=d.humidity()

    t_str=str(t)
    h_str=str(h)

    # adc
    adc_val=adc.read()

    # form payload
    payload ={"temp_ambient": t,"humidity_ambient":h,"adc_val":adc_val}

    # add in onewire


    oled.fill(0)
    oled.text("  Q U A H O G",0,0)
    oled.text("t:"+t_str+";h:"+h_str,0,20)
    oled.text("a:"+str(adc_val),0,30)
    oled.text(oled_str,0,40)

    oled.text("Connect wifi...",0,50)
    oled.show()


    do_connect(essid,psswd)


    oled.fill(0)
    oled.text("  Q U A H O G",0,0)
    oled.text("t:"+t_str+";h:"+h_str,0,20)
    oled.text("a:"+str(adc_val),0,30)
    oled.text(oled_str,0,40)

    oled.text("Posting...",0,50)
    oled.show()


    status=post_data(payload)


    oled.fill(0)
    oled.text("  Q U A H O G",0,0)
    oled.text("t:"+t_str+";h:"+h_str,0,20)
    oled.text("a:"+str(adc_val),0,30)
    oled.text(oled_str,0,40)

    oled.text("Status:"+str(status),0,50)
    oled.show()
    
    time.sleep(5)






