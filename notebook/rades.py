import ujson as json
import urequests as requests
import time
import dht
import machine
from machine import Pin
import ssd1306
from machine import I2C
import onewire, ds18x20
from machine import SPI
from upy_rfm9x import RFM9x

TIMEOUT=20

#radio
sck=Pin(25)
mosi=Pin(33)
miso=Pin(32)
cs = Pin(26, Pin.OUT)
resetNum=27
spi=SPI(1,baudrate=5000000,sck=sck,mosi=mosi,miso=miso)
rfm9x = RFM9x(spi, cs, resetNum, 915.0)

index=0

i2c = I2C(-1, Pin(14), Pin(2))
oled = ssd1306.SSD1306_I2C(128, 64, i2c)


oled.fill(0)
oled.text("  Q U A H O G",0,0)
oled.show()
time.sleep(1)

#url='http://192.168.1.3:5000'
url='http://172.16.10.119:5000'
#url='http://mosspig.local:5000'
headers = {'Content-type':'application/json', 'Accept':'application/json'}

essid="Artisan\'s Asylum"
#essid="NETGEAR57"
psswd="learn.make.teach"
#psswd="curlyorchestra497"
print(essid,psswd)



oled.fill(0)
oled.text("  Q U A H O G",0,0)
oled.text("Read sensors...",0,50)
oled.show()
time.sleep(1)


# SENSOR SETUP
dat = machine.Pin(5)
d = dht.DHT22(machine.Pin(18))



adc = machine.ADC(machine.Pin(35))

oled_str=" "


def post_data(payload):
    try:
    	r = requests.post(url,data=json.dumps(payload),headers=headers)
    except Exception as e:
	print(e)

	#r.close()

	return "timeout"

    else:

	r.close()

	print('Status', r.status_code)

   	return r.status_code



def do_connect(essid,psswd):
    import network
    sta_if = network.WLAN(network.STA_IF)
    if not sta_if.isconnected():
        print('connecting to network...')
        sta_if.active(False)
        sta_if.active(True)
        sta_if.connect(essid, psswd)
        while not sta_if.isconnected():
            pass
    print('network config:', sta_if.ifconfig())


while True:
    
    #dht22:
    d.measure()
    t=d.temperature()
    h=d.humidity()

    t_str=str(t)
    h_str=str(h)

    # adc
    adc_val=adc.read()

    # form payload
    payload ={"temp_ambient": t,"humidity_ambient":h,"adc_val":adc_val}

    rfm9x.receive(timeout=TIMEOUT)
    if rfm9x.packet is not None:
        print(rfm9x.packet)
        raw=str(rfm9x.packet).split('\x00')
        #nextraw=raw[0].split('\"')
        #print(nextraw.split('\\'))
        try:
            txt=str(rfm9x.packet,'ascii')
            print("txt=",txt)
            params=txt.strip("\x00").split(",")
            print('params=',params)
            #othertxt=str(rfm9x.packet,'ascii',errors='ignore')
            #print('othertxt=',othertxt)
            va=float(params[0])
            vb=float(params[1])
            vc=float(params[2])
            payload ={"va":va,"vb":vb,"vc":vc,"temp_ambient": t,"humidity_ambient":h,"adc_val":adc_val}
        except Exception as e:
            print(e)    
    else:
        print('radio timed out')

    oled.fill(0)
    oled.text("  Q U A H O G",0,0)
    oled.text("t:"+t_str+";h:"+h_str,0,20)
    oled.text("a:"+str(adc_val),0,30)
    oled.text(oled_str,0,40)

    oled.text("Connect wifi...",0,50)
    oled.show()


    do_connect(essid,psswd)


    oled.fill(0)
    oled.text("  Q U A H O G",0,0)
    oled.text("t:"+t_str+";h:"+h_str,0,20)
    oled.text("a:"+str(adc_val),0,30)
    oled.text(oled_str,0,40)

    oled.text("Posting...",0,50)
    oled.show()


    status=post_data(payload)


    oled.fill(0)
    oled.text("  Q U A H O G",0,0)
    oled.text("t:"+t_str+";h:"+h_str,0,20)
    oled.text("a:"+str(adc_val),0,30)
    oled.text(oled_str,0,40)

    oled.text("Status:"+str(status),0,50)
    oled.show()
    
    time.sleep(5)






