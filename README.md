# escargot

## novnc

https://www.raspberrypi.org/forums/viewtopic.php?p=1370750


https://github.com/novnc/noVNC

https://docs.dataplicity.com/docs/make-a-remote-graphical-interface-for-your-pi

basically opted to use tightvnc and novnc
they are both running as services

novnc was installed /usr/local/share/noVNC via git clone
had to copy vnc.html to index.html so that there would be a default startup (could change to 'lite' version)

the relevant startup scripts are:

```
/etc/systemd/system/tightvnc@.service
/etc/systemd/system/novnc@.service
```

which are enabled / disable with e.g.

```
systemctl (start/stop) tightvnc@1.service
systemctl (enable|disable) tightvnc@1.service
```

note that there are further instructions here if you want the X11 displays to be 'mirrored' rather than start up separate processes: https://www.raspberrypi.org/forums/viewtopic.php?p=1370750


https://www.digitalocean.com/community/tutorials/how-to-use-systemctl-to-manage-systemd-services-and-units

ah, note: needed to make an edit. new configuration files are:

/etc/systemd/system/tightvnc@.service:

```
[Unit]
Description=Remote desktop service (VNC)
After=syslog.target network.target

[Service]
Type=forking
User=pi
PAMName=login
PIDFile=/home/pi/.vnc/%H:%i.pid
ExecStartPre=-/usr/bin/vncserver -kill :%i > /dev/null 2>&1
ExecStart=/usr/bin/vncserver -depth 24 -geometry 1280x800 :%i

[Install]
WantedBy=multi-user.target
```

and

/etc/systemd/system/novnc@.service:

```
[Unit]
Description=noVNC Service
After=x11vnc.service

[Service]
User=pi
PAMName=login
ExecStart=/usr/local/share/noVNC/utils/launch.sh --vnc localhost:590%i

[Install]
WantedBy=multi-user.target
```

## Pi OLED
https://learn.adafruit.com/adafruit-pioled-128x32-mini-oled-for-raspberry-pi/usage

## Changing the hostname

pi@mosspig.local
noVNC password: mosspig

https://www.howtogeek.com/167195/how-to-change-your-raspberry-pi-or-other-linux-devices-hostname/
