module.exports = function formatData(post){
  const time = Math.round((new Date).getTime() / 1000)

  return Object.assign(
    {
      timestamp: time,
      //uploaded: false
    },
    post
  )
}
